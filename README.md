# OptionSelectionMSTestProblem

I have unit tests which will pass when run together, yet individually they pass.
I've remove all static values, and everything that I can see that would be state-full between each test.
I'd be very grateful if someone could please have a look at this